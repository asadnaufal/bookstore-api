INSERT INTO authors(first_name, last_name) VALUES ('Sigmund', 'Freud'), ('Carl', 'Jung'), ('William', 'Shakespeare'), ('Voltaire', '');

INSERT INTO publishers(name) VALUES ('Penguin Random House'), ('Picador'), ('Dover publication');

INSERT INTO books(author_id, publisher_id, isbn, title, total_pages, cover_type) VALUES
(3, 1,  77818920, 'Macbeth', 440, 'softcover'), (2, 1, 3333131, 'Liber Novus', 330, 'hardcover'), (4, 3, 0486266893, 'Candide', 112, 'softcover'),(1, 1, 8888888, 'Interpretation Of Dream', '480', 'hardcover');

INSERT INTO price_list(book_id, price, discount, valid_from, valid_to) VALUES (1, 375000, 70, '2020-10-02', '2020-10-28'), (2, 2366000, 43, '2020-10-02', '2021-10-28'), (3, 97000, 20, '2020-10-02', '2021-10-28'), (4, 497000, 51, '2020-10-02', '2021-10-28');

INSERT INTO genres(name)
VALUES ('essay'), ('science'), ('classic'), ('psychology'), ('romance'), ('biography'), ('poetry');

INSERT INTO book_genres(book_id, genre_id)
VALUES (1, 3),(1, 7),(2, 4),(3, 3),(4, 4);
