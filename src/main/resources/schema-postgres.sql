DROP TABLE IF EXISTS price_list;
DROP TABLE IF EXISTS book_genres;
DROP TABLE IF EXISTS genres;
DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS authors;
DROP TABLE IF EXISTS publishers;
DROP TYPE IF EXISTS cover_type;

CREATE TABLE authors (
  id serial PRIMARY KEY,
  first_name VARCHAR (50) NOT NULL,
  last_name VARCHAR (50)
);

CREATE TABLE publishers (
  id serial PRIMARY KEY,
  name VARCHAR(150)
);

CREATE TYPE cover_type AS ENUM ('hardcover', 'softcover');

CREATE TABLE books (
  id serial PRIMARY KEY,
  author_id INT,
  publisher_id INT,
  isbn INT,
  title VARCHAR(250),
  total_pages VARCHAR(250),
  cover_type cover_type,
  CONSTRAINT fk_author FOREIGN KEY(author_id) REFERENCES authors(id),
  CONSTRAINT fk_publisher FOREIGN KEY(publisher_id) REFERENCES publishers(id)
);

CREATE TABLE genres (
  id serial PRIMARY KEY,
  name VARCHAR (50)
);

CREATE TABLE book_genres (
  id serial PRIMARY KEY,
  book_id INT,
  genre_id INT,
  CONSTRAINT fk_book FOREIGN KEY(book_id) REFERENCES books(id),
  CONSTRAINT fk_genre FOREIGN KEY(genre_id) REFERENCES genres(id)
);

CREATE TABLE price_list (
  id serial PRIMARY KEY,
  book_id INT UNIQUE,
  price NUMERIC(12, 2),
  discount NUMERIC(4, 2) CHECK (discount <= 100),
  discounted_price NUMERIC(12, 2) GENERATED ALWAYS AS (price - (price * discount / 100)) STORED,
  valid_from DATE NOT NULL,
	valid_to DATE NOT NULL,
  CONSTRAINT fK_book_id FOREIGN KEY(book_id) REFERENCES books(id),
  CONSTRAINT valid_range_check CHECK (valid_to >= valid_from)
);