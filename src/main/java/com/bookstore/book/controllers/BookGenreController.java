package com.bookstore.book.controllers;

import java.util.List;

import com.bookstore.genre.models.Genre;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/book-genres")
public class BookGenreController {

  @GetMapping("/{bookId}")
  public List<Genre> getBookGenres(Long bookId) {
    return null;
  }
}
