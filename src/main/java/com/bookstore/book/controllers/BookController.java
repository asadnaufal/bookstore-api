package com.bookstore.book.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.bookstore.book.dtos.CreateBookDTO;
import com.bookstore.book.models.Book;
import com.bookstore.book.models.BookGenre;
import com.bookstore.book.services.IBookGenreService;
import com.bookstore.book.services.IBookService;
import com.bookstore.genre.models.Genre;
import com.bookstore.pricelist.models.BookPrice;
import com.bookstore.pricelist.services.PriceListService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/books")
public class BookController {

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private IBookService bookservice;

  @Autowired
  private IBookGenreService bookGenresService;

  @Autowired
  private PriceListService priceListService;

  @GetMapping
  @ResponseBody
  public CollectionModel<Book> getBooks(@RequestParam Optional<String> searchKey,
      @RequestParam Optional<List<String>> genres, @RequestParam(name = "with") Optional<List<String>> relations) {
    Map<String, Object> options = new HashMap<>();

    searchKey.ifPresent(value -> options.put("searchKey", value));
    genres.ifPresent(value -> options.put("genres", value));
    relations.ifPresent(value -> options.put("relations", value));

    List<Book> books = bookservice.findAll(!options.isEmpty() ? Optional.of(options) : Optional.empty());

    Link link = linkTo(methodOn(BookController.class).getBooks(searchKey, genres, relations)).withSelfRel();

    CollectionModel<Book> result = CollectionModel.of(books, link);

    return result;
  }

  @GetMapping(value = "/{bookId}")
  public ResponseEntity<Book> getBookDetail(@PathVariable(name = "bookId") Long bookId) {
    Optional<Book> book = bookservice.findOneById(bookId);

    return ResponseEntity.of(book);
  }

  private <BookDTO> Book convDtoToModel(BookDTO dto) {
    return modelMapper.map(dto, Book.class);
  }

  @PostMapping
  public Book createBook(@RequestBody CreateBookDTO book) {

    Book createdBook = bookservice.createBook(convDtoToModel(book));

    book.getGenres().ifPresent(bookGenres -> {
      List<BookGenre> _bookGenres = bookGenres.stream().map(bg -> {
        return new BookGenre(createdBook.getId().intValue(), bg.intValue());
      }).collect(Collectors.toList());
      
      bookGenresService.createBookGenreBatch(_bookGenres);
    });

    book.getPrice().ifPresent(price -> {
      BookPrice bookPrice = new BookPrice(); 
      bookPrice.setBookId(createdBook.getId());
      bookPrice.setPrice(price);

      priceListService.addBookPrice(bookPrice);
    });

    return createdBook;
  }

  @GetMapping("/{bookId}/genres")
  public List<Genre> getBookGenres(@PathVariable Long bookId) {
    return bookGenresService.getBookGenres(bookId);
  }
}