package com.bookstore.book.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "book_genres")
public class BookGenre {
  private Long id;
  private int bookId;
  private int genreId;

  public BookGenre(int bookId, int genreId) {
    this.bookId = bookId;
    this.genreId = genreId;
  }

  @Id
  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Column(name = "book_id")
  public int getBookId() {
    return this.bookId;
  }

  public void setBookId(int bookId) {
    this.bookId = bookId;
  }

  @Column(name = "genre_id")
  public int getGenreId() {
    return this.genreId;
  }

  public void setGenreId(int genreId) {
    this.genreId = genreId;
  }

}
