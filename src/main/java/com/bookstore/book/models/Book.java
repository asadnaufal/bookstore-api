package com.bookstore.book.models;

import java.util.List;

import com.bookstore.author.models.Author;
import com.bookstore.genre.models.Genre;
import com.bookstore.publisher.models.Publisher;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import org.springframework.hateoas.RepresentationModel;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.*;

/* enum CoverType {
  SOFTCOVER,
  HARDCOVER
} */

@Entity
@Table(name = "books")
@JsonIgnoreProperties({ "publisherId", "authorId" })
public class Book extends RepresentationModel<Book> {
  private Long id;
  private int authorId;
  private int publisherId;
  private int isbn;
  private String title;
  private int totalPages;
  private String coverType;
  private Float price;
  private Float discount;
  private Float discountedPrice;
  private List<Genre> genres;
  private Publisher publisher;
  private Author author;

  public Book() {
  }

  public Book(ResultSet rs) throws SQLException {
    this.id = rs.getLong("id");
    this.authorId = rs.getInt("author_id");
    this.publisherId = rs.getInt("publisher_id");
    this.isbn = rs.getInt("isbn");
    this.title = rs.getString("title");
    this.totalPages = rs.getInt("total_pages");
    this.coverType = rs.getString("cover_type");
    // this.price = rs.getFloat("price");
    // this.discount = rs.getFloat("discount");
    // this.discountedPrice = rs.getFloat("discounted_price");
  }

  public Book(ResultSet rs, boolean withPrice) throws SQLException {
    this(rs);
    if (withPrice) {
      this.price = rs.getFloat("price");
      this.discount = rs.getFloat("discount");
      this.discountedPrice = rs.getFloat("discounted_price");
    }
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Column(name = "author_id")
  public int getAuthorId() {
    return this.authorId;
  }

  public void setAuthorId(int authorId) {
    this.authorId = authorId;
  }

  @Column(name = "publisher_id")
  public int getPublisherId() {
    return this.publisherId;
  }

  public void setPublisherId(int publisherId) {
    this.publisherId = publisherId;
  }

  @Column(name = "isbn")
  public int getIsbn() {
    return this.isbn;
  }

  public void setIsbn(int isbn) {
    this.isbn = isbn;
  }

  @Column(name = "title")
  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Column(name = "total_pages")
  public int getTotalPages() {
    return this.totalPages;
  }

  public void setTotalPages(int totalPages) {
    this.totalPages = totalPages;
  }

  @Column(name = "cover_type")
  public String getCoverType() {
    return this.coverType;
  }

  public void setCoverType(String coverType) {
    this.coverType = coverType;
  }

  @Transient
  @JsonInclude
  public Float getPrice() {
    return this.price;
  }

  public void setPrice(Float price) {
    this.price = price;
  }

  @Transient
  @JsonInclude
  public Float getDiscount() {
    return this.discount;
  }

  public void setDiscount(Float discount) {
    this.discount = discount;
  }

  @Transient
  @JsonInclude
  public Float getDiscountedPrice() {
    return this.discountedPrice;
  }

  public void setDiscountedPrice(Float discountedPrice) {
    this.discountedPrice = discountedPrice;
  }

  @Transient
  @JsonInclude()
  public List<Genre> getGenres() {
    return this.genres;
  }

  public void setGenres(List<Genre> genres) {
    this.genres = genres;
  }

  public void addGenre(Genre genre) {
    this.genres.add(genre);
  }

  @Transient
  @JsonInclude()
  public Publisher getPublisher() {
    return this.publisher;
  }

  public void setPublisher(Publisher publisher) {
    this.publisher = publisher;
  }

  @Transient
  @JsonInclude
  public Author getAuthor() {
    return this.author;
  }

  public void setAuthor(Author author) {
    this.author = author;
  }

}