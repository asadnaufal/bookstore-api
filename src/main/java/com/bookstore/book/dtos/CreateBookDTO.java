package com.bookstore.book.dtos;

import java.util.List;
import java.util.Optional;

public class CreateBookDTO {
  private int authorId;
  private int publisherId;
  private int isbn;
  private String title;
  private int totalPages;
  private String coverType;
  private Optional<List<Long>> genres;
  private Optional<Float> price;
  private Optional<Float> discount;

  public int getAuthorId() {
    return this.authorId;
  }

  public void setAuthorId(int authorId) {
    this.authorId = authorId;
  }

  public int getPublisherId() {
    return this.publisherId;
  }

  public void setPublisherId(int publisherId) {
    this.publisherId = publisherId;
  }

  public int getIsbn() {
    return this.isbn;
  }

  public void setIsbn(int isbn) {
    this.isbn = isbn;
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getTotalPages() {
    return this.totalPages;
  }

  public void setTotalPages(int totalPages) {
    this.totalPages = totalPages;
  }

  public String getCoverType() {
    return this.coverType;
  }

  public void setCoverType(String coverType) {
    this.coverType = coverType;
  }

  public Optional<List<Long>> getGenres() {
    return this.genres;
  }

  public void setGenres(Optional<List<Long>> genres) {
    this.genres = genres;
  }

  public Optional<Float> getPrice() {
    return this.price;
  }

  public void setPrice(Optional<Float> price) {
    this.price = price;
  }

  public Optional<Float> getDiscount() {
    return this.discount;
  }

  public void setDiscount(Optional<Float> discount) {
    this.discount = discount;
  }
}