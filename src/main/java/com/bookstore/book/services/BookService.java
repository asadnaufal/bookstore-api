package com.bookstore.book.services;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.bookstore.book.repositories.JdbcBookRepository;
import com.bookstore.book.models.Book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService implements IBookService {
  @Autowired
  private JdbcBookRepository bookRepository;

  @Override
  public List<Book> findAll(Optional<Map<String, Object>> options) {
    return bookRepository.findAll(options);
  }

  @Override
  public List<Book> findBooksByAuthorId(Long authorId) {
    return bookRepository.findBooksByAuthorId(authorId);
  }

  @Override
  public Optional<Book> findOneById(Long id) {
    return bookRepository.findOneById(id);
  }

  @Override
  public Book createBook(Book book) {
    return bookRepository.createBook(book);
  }
}