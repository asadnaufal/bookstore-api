package com.bookstore.book.services;

import java.util.List;

import com.bookstore.book.models.BookGenre;
import com.bookstore.book.repositories.JdbcBookGenreRepository;
import com.bookstore.genre.models.Genre;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookGenreService implements IBookGenreService {
  @Autowired
  private JdbcBookGenreRepository jdbcBookGenreRepository;

  @Override
  public List<Genre> getBookGenres(Long bookId) {
    return jdbcBookGenreRepository.getBookGenres(bookId);
  }

  @Override
  public Integer createBookGenreBatch(List<BookGenre> bookGenres) {
    return jdbcBookGenreRepository.createBookGenreBatch(bookGenres);
  }

}
