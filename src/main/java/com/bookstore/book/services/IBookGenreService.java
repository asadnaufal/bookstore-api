package com.bookstore.book.services;

import java.util.List;

import com.bookstore.book.models.BookGenre;
import com.bookstore.genre.models.Genre;

public interface IBookGenreService {
  List<Genre> getBookGenres(Long bookId);
  Integer createBookGenreBatch(List<BookGenre> bookGenres);
}

