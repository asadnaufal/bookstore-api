package com.bookstore.book.services;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.bookstore.book.models.Book;

public interface IBookService {
  List<Book> findAll(Optional<Map<String, Object>> options);
  List<Book> findBooksByAuthorId(Long authorId);
  Optional<Book> findOneById(Long id);
  Book createBook(Book book);
}