package com.bookstore.book.repositories;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import com.bookstore.book.models.Book;

public interface BookRepository {
  List<Book> findAll(Optional<Map<String, Object>> options);
  List<Book> findBooksByAuthorId(Long authorId);
  Optional<Book> findOneById(Long id);
  Book createBook(Book book);
}