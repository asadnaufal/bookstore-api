package com.bookstore.book.repositories;

import com.bookstore.author.models.Author;
import com.bookstore.author.repositories.JdbcAuthorRepository;
import com.bookstore.book.models.Book;
import com.bookstore.genre.models.Genre;
import com.bookstore.publisher.models.Publisher;
import com.bookstore.publisher.repositories.JdbcPublisherRepository;

import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcBookRepository implements BookRepository {
  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Autowired
  private JdbcAuthorRepository authorRepository;

  @Autowired
  private JdbcBookGenreRepository bookGenreRepository;

  @Autowired
  private JdbcPublisherRepository publisherRepository;

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public List<Book> findAll(Optional<Map<String, Object>> options) {
    String sql = "SELECT books.*, p.price, p.discounted_price, p.discount FROM books JOIN price_list AS p ON (books.id = p.book_id)";

    List<Book> books = new ArrayList<>();

    Map<String, Object> opts = new HashMap<>();

    if (options.isPresent()) {
      opts = options.get();
    }

    if (opts.containsKey("searchKey") || opts.containsKey("genres")) {
      String searchKey = (String) opts.getOrDefault("searchKey", "");

      if (searchKey.length() > 0) {
        String SELECT_SEARCHABLE_FIELDS = "WITH search_books AS (SELECT books.*, LOWER(CONCAT_WS(' ', books.title, authors.first_name, authors.last_name, publishers.name)) AS searchable_field FROM books LEFT JOIN authors ON (books.author_id = authors.id) LEFT JOIN publishers ON (publishers.id = books.publisher_id))";

        String SELECT_BOOKS = "SELECT id, author_id, publisher_id, isbn, title, total_pages, cover_type FROM search_books";

        String WHERE_CLAUSE = "WHERE searchable_field LIKE LOWER(CONCAT('%', ?, '%'))";

        sql = String.format("%s %s %s", SELECT_SEARCHABLE_FIELDS, SELECT_BOOKS, WHERE_CLAUSE);
      }

      @SuppressWarnings("unchecked")
      List<String> selectedGenres = (List<String>) opts.getOrDefault("genres", new ArrayList<>());

      if (selectedGenres.size() > 0) {
        String SELECTED_GENRES = sql.concat(" ").concat("JOIN book_genres ON book_genres.book_id = books.id JOIN genres ON genres.id = book_genres.genre_id WHERE");

        for (String genre : selectedGenres) {
          SELECTED_GENRES = SELECTED_GENRES.concat(" ").concat("genres.name = ?");
          if (selectedGenres.indexOf(genre) < selectedGenres.size() - 1) {
            SELECTED_GENRES = SELECTED_GENRES.concat(" ").concat("OR");
          }
        }

        sql = String.format("%s %s %s", sql, "INTERSECT", SELECTED_GENRES);
      }
      PreparedStatementCallback<List<Book>> psc = new PreparedStatementCallback<List<Book>>() {
        @Override
        public List<Book> doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
          int idx = 1;
          int selectedGenresSize = selectedGenres.size();

          if (!searchKey.isEmpty()) {
            idx += 1;
            selectedGenresSize += 1;
            ps.setString(1, searchKey);
          }

          if (selectedGenres.size() > 0) {
            for (int i = idx; i <= selectedGenresSize; i++) {
              ps.setString(i, selectedGenres.get(i - idx));
            }
          }

          ResultSet rs = ps.executeQuery();

          List<Book> _books = new ArrayList<>();

          while (rs.next()) {
            _books.add(new Book(rs, true));
          }

          return _books;
        }
      };

      books = jdbcTemplate.execute(sql, psc);

    } else {
      books = jdbcTemplate.query(sql, (rs, rowNum) -> new Book(rs, true));
    }

    if (opts.containsKey("relations")) {
      Map<String, Object> opt = options.get();

      @SuppressWarnings("unchecked")
      List<String> relations = (List<String>) opt.getOrDefault("relations", new ArrayList<>());

      if (relations.size() > 0) {
        for (Book book : books) {
          PreparedStatementCallback<List<Genre>> psc = new PreparedStatementCallback<List<Genre>>() {
            public List<Genre> doInPreparedStatement(PreparedStatement ps) throws SQLException {
              ps.setInt(1, book.getId().intValue());
              ResultSet rs = ps.executeQuery();

              List<Genre> _genres = new ArrayList<Genre>();
              while (rs.next()) {
                _genres.add(new Genre(rs));
              }

              return _genres;
            }
          };

          if (relations.contains("genres")) {
            String SELECT_GENRES_FROM_BOOK = "SELECT genres.id, genres.name FROM genres LEFT JOIN book_genres ON (genres.id = book_genres.genre_id) where book_id = ?";

            List<Genre> genres = jdbcTemplate.execute(SELECT_GENRES_FROM_BOOK, psc);

            book.setGenres(genres);
          }

          if (relations.contains("publisher")) {
            String SELECT_PUBLISHER_FROM_BOOK = "SELECT * FROM publishers WHERE id = ?";

            Publisher publisher = jdbcTemplate.queryForObject(SELECT_PUBLISHER_FROM_BOOK,
                new Object[] { book.getPublisherId() }, (rs, rn) -> new Publisher(rs));

            book.setPublisher(publisher);

          }

          if (relations.contains("author")) {
            String SELECT_AUTHOR_FROM_BOOK = "SELECT * FROM authors WHERE id = ?";

            Author author = jdbcTemplate.queryForObject(SELECT_AUTHOR_FROM_BOOK, new Object[] { book.getAuthorId() },
                (rs, rn) -> new Author(rs));

            book.setAuthor(author);
          }
        }
      }
    }

    return books;
  }

  @Override
  public List<Book> findBooksByAuthorId(Long id) {
    String sql = "SELECT * FROM books WHERE author_id = ?";
    return jdbcTemplate.query(sql, new Object[] { id }, (rs, rowNum) -> new Book(rs));
  }

  @Override
  public Optional<Book> findOneById(Long id) {
    String sql = "SELECT * FROM books WHERE id = ?";

    PreparedStatementCallback<Optional<Book>> psc = new PreparedStatementCallback<Optional<Book>>() {
      @Override
      public Optional<Book> doInPreparedStatement(PreparedStatement ps) throws SQLException {
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();

        Optional<Book> book = Optional.empty();
        while (rs.next()) {
          book = Optional.of(new Book(rs));
        }

        if (book.isPresent()) {
          Book _book = book.get();
          Optional<Author> author = authorRepository.findOne(Long.valueOf(_book.getAuthorId()));
          _book.setAuthor(author.get());

          List<Genre> genres = bookGenreRepository.getBookGenres(_book.getId());
          _book.setGenres(genres);

          Optional<Publisher> publisher = publisherRepository.findOneById(Long.valueOf(_book.getPublisherId()));
          _book.setPublisher(publisher.get());

          return Optional.of(_book);
        }

        return book;
      }
    };

    return jdbcTemplate.execute(sql, psc);
  }

  @Override
  public Book createBook(Book book) {
    String sql = "INSERT INTO books(author_id, publisher_id, isbn, title, total_pages, cover_type) VALUES ((SELECT id FROM authors WHERE id = ?), (SELECT id from publishers WHERE id = ?), ?, ?, ?, ?::cover_type);";

    KeyHolder keyHolder = new GeneratedKeyHolder();

    jdbcTemplate.update(connection -> {
      PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      ps.setInt(1, book.getAuthorId());
      ps.setInt(2, book.getPublisherId());
      ps.setInt(3, book.getIsbn());
      ps.setString(4, book.getTitle());
      ps.setInt(5, book.getTotalPages());
      ps.setString(6, book.getCoverType());

      return ps;
    }, keyHolder);

    Book newBook = book;

    Map<String, Object> newGeneratedKeys = keyHolder.getKeys();

    Integer newBookId = (Integer) newGeneratedKeys.get("id");

    newBook.setId(newBookId.longValue());

    return newBook;
  }

}