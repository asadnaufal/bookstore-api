package com.bookstore.book.repositories;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.bookstore.book.models.BookGenre;
import com.bookstore.genre.models.Genre;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcBookGenreRepository implements BookGenreRepository {

  @Autowired
  JdbcTemplate jdbcTemplate;

  public JdbcTemplate getJdbcTemplate() {
    return this.jdbcTemplate;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public List<Genre> getBookGenres(Long bookId) {
    String sql = "SELECT genres.id, genres.name FROM genres INNER JOIN book_genres ON (book_genres.genre_id = genres.id) WHERE book_genres.book_id = ?";

    return jdbcTemplate.query(sql, new Object[] { bookId }, (rs, rowNum) -> new Genre(rs));
  }

  Integer doCreateBookGenre(BookGenre bookGenre) {
    String sql = "INSERT INTO book_genres(book_id, genre_id) VALUES(?, ?)";

    PreparedStatementCallback<Integer> psc = new PreparedStatementCallback<Integer>() {
      @Override
      public Integer doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
        ps.setInt(1, bookGenre.getBookId());
        ps.setInt(2, bookGenre.getGenreId());
        Integer rowUpdated = ps.executeUpdate();
        return rowUpdated;
      };
    };

    return jdbcTemplate.execute(sql, psc);
  }

  @Override
  public Integer createBookGenreBatch(List<BookGenre> bookGenres) {
    Integer rowUpdated = 0;
    for (BookGenre bookGenre : bookGenres) {
      rowUpdated += doCreateBookGenre(bookGenre);
    }

    return rowUpdated;
  }
}
