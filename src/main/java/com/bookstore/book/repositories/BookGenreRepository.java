package com.bookstore.book.repositories;

import java.util.List;

import com.bookstore.book.models.BookGenre;
import com.bookstore.genre.models.Genre;

public interface BookGenreRepository {
  List<Genre> getBookGenres(Long bookId);
  Integer createBookGenreBatch(List<BookGenre> Genres);
}
