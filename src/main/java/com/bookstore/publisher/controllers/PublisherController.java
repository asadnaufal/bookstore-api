package com.bookstore.publisher.controllers;

import java.util.List;

import com.bookstore.book.models.Book;
import com.bookstore.publisher.services.IPublisherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/publishers")
public class PublisherController {
 @Autowired
 private IPublisherService publisherService; 

 @GetMapping("/{publisherId}/books")
 public List<Book> getBooks(@PathVariable("publisherId") Long publisherId) {
  return publisherService.findPublisherBooks(publisherId);
 }

}
