package com.bookstore.publisher.services;

import java.util.List;

import com.bookstore.book.models.Book;
import com.bookstore.publisher.repositories.JdbcPublisherRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PublisherService implements IPublisherService {
  @Autowired
  private JdbcPublisherRepository publisherRepository;

  @Override
  public List<Book> findPublisherBooks(Long publisherId) {
    return publisherRepository.findPublisherBooks(publisherId);
  }
  
}
