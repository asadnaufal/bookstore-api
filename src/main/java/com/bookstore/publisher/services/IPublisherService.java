package com.bookstore.publisher.services;

import java.util.List;

import com.bookstore.book.models.Book;

public interface IPublisherService {
  List<Book> findPublisherBooks(Long publisherId);
}
