package com.bookstore.publisher.repositories;

import java.util.List;
import java.util.Optional;

import com.bookstore.book.models.Book;
import com.bookstore.publisher.models.Publisher;

public interface PublisherRepository {
  List<Book> findPublisherBooks(Long publisherId);
  Optional<Publisher> findOneById(Long publisherId);
}
