package com.bookstore.publisher.repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import com.bookstore.book.models.Book;
import com.bookstore.publisher.models.Publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcPublisherRepository implements PublisherRepository {

  @Autowired
  JdbcTemplate jdbcTemplate;

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public List<Book> findPublisherBooks(Long publisherId) {
    String sql = "SELECT * FROM books RIGHT JOIN publishers on (books.publisher_id = publishers.id) WHERE publishers.id = ?";
    PreparedStatementSetter pss = new PreparedStatementSetter() {
      public void setValues(PreparedStatement ps) throws SQLException {
        ps.setInt(1, publisherId.intValue());
      }
    };
    return jdbcTemplate.query(sql, pss, (rs, rowNumber) -> new Book(rs));
  }

@Override
public Optional<Publisher> findOneById(Long publisherId) {
  String sql = "SELECT * FROM publishers WHERE id = ?";

  PreparedStatementCallback<Optional<Publisher>> psc = new PreparedStatementCallback<Optional<Publisher>>(){
    @Override
    public Optional<Publisher> doInPreparedStatement(PreparedStatement ps) throws SQLException {
      Optional<Publisher> publisher = Optional.empty(); 

      ps.setInt(1, publisherId.intValue());
      ResultSet rs = ps.executeQuery();

      while(rs.next()) {
        publisher = Optional.of(new Publisher(rs));
      }

      return publisher;
    }
  };

  return jdbcTemplate.execute(sql, psc);
}
}
