package com.bookstore.publisher.models;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "publishers")
public class Publisher {
  private Long id;
  private String name;

  public Publisher(ResultSet rs) throws SQLException { 
    this.id = rs.getLong("id");
    this.name = rs.getString("name");
  }

  @Id
  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Column(name = "name")
  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
