package com.bookstore.pricelist.services.impl;

import com.bookstore.pricelist.models.BookPrice;
import com.bookstore.pricelist.repositories.PriceListRepository;
import com.bookstore.pricelist.services.PriceListService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PriceListServiceImpl implements PriceListService {
  @Autowired
  private PriceListRepository priceListRepository;

  @Override
  public Integer addBookPrice(BookPrice bookPrice) {
    return priceListRepository.addBookPrice(bookPrice);
  }
}
