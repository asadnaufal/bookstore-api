package com.bookstore.pricelist.services;

import com.bookstore.pricelist.models.BookPrice;

public interface PriceListService {
  Integer addBookPrice(BookPrice bookPrice);
}
