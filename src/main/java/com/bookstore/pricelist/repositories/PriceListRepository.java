package com.bookstore.pricelist.repositories;

import com.bookstore.pricelist.models.BookPrice;

public interface PriceListRepository {
  Integer addBookPrice(BookPrice bookPrice);
}
