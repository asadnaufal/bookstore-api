package com.bookstore.pricelist.repositories.impl;

import java.sql.PreparedStatement;

import com.bookstore.pricelist.models.BookPrice;
import com.bookstore.pricelist.repositories.PriceListRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class PriceListRepositoryImpl implements PriceListRepository {
  @Autowired
  private JdbcTemplate jdbcTemplate;

  public JdbcTemplate getJdbcTemplate() {
    return this.jdbcTemplate;
  }

  @Override
  public Integer addBookPrice(BookPrice bookPrice) {
    String sql = "INSERT INTO price_list(book_id, price, discount, valid_from, valid_to) VALUES(?, ?, ?, ?, ?)";

    KeyHolder keyHolder = new GeneratedKeyHolder();

    Integer rowUpdated = jdbcTemplate.update(connection -> {
      PreparedStatement ps = connection.prepareStatement(sql);
      ps.setInt(1, bookPrice.getBookId().intValue());
      ps.setFloat(2, bookPrice.getPrice());
      ps.setFloat(3, bookPrice.getDiscount());
      ps.setDate(4, bookPrice.getValidFrom());
      ps.setDate(5, bookPrice.getValidTo());

      return ps;
    }, keyHolder);

    return rowUpdated;
  }
}
