package com.bookstore.pricelist.models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "price_list")
public class BookPrice {
  private Long id;
  private Long bookId;
  private Float price;
  private Float discount;
  private Float discountedPrice;
  private Date validFrom;
  private Date validTo;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getBookId() {
    return this.bookId;
  }

  public void setBookId(Long bookId) {
    this.bookId = bookId;
  }

  public Float getPrice() {
    return this.price;
  }

  public void setPrice(Float price) {
    this.price = price;
  }

  public Float getDiscount() {
    return this.discount;
  }

  public void setDiscount(Float discount) {
    this.discount = discount;
  }

  public Float getDiscountedPrice() {
    return this.discountedPrice;
  }

  public void setDiscountedPrice(Float discountedPrice) {
    this.discountedPrice = discountedPrice;
  }

  public Date getValidFrom() {
    return this.validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return this.validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

}
