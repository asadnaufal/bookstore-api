package com.bookstore.author.controllers;

import java.util.List;
import java.util.Optional;

import com.bookstore.author.services.IAuthorService;
import com.bookstore.author.models.Author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authors")
public class AuthorController {

  @Autowired
  private IAuthorService authorService;

  @GetMapping
  public List<Author> getAuthors() {
    return authorService.findAll();
  }

  @GetMapping(value = "/{authorId}")
  public Optional<Author> getAuthorById(@PathVariable("authorId") Long authorId) {
    return authorService.findOne(authorId);
  }

  @PostMapping
  public Optional<Author> createAuthor(@RequestBody Author author) {
    return authorService.create(author);
  }

  @DeleteMapping(value = "/{authorId}")
  public Optional<Author> deleteAuthor(@PathVariable("authorId") Long id) {
    return authorService.delete(id);
  }

  @PutMapping(value = "/{authorId}")
  public Optional<Author> updateAuthor(@PathVariable("authorId") Long id, @RequestBody Author author) {
    return authorService.update(id, author);
  }
}
