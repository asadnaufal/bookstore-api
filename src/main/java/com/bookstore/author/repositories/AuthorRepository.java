package com.bookstore.author.repositories;

import java.util.List;
import java.util.Optional;

import com.bookstore.author.models.Author;

public interface AuthorRepository {
  List<Author> findAll();
  Integer create(Author authorData);
  Optional<Author> findOne(Long id);
  Integer delete(Long id);
  Integer update(Long authorId, Author authorData);
  Integer update(Author authorData);
}
