package com.bookstore.author.repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import com.bookstore.author.models.Author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcAuthorRepository implements AuthorRepository {
  @Autowired
  private JdbcTemplate jdbcTemplate;

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public List<Author> findAll() {
    String SELECT_AUTHORS = "SELECT * FROM authors";
    return jdbcTemplate.query(SELECT_AUTHORS, (rs, rn) -> new Author(rs));
  }

  @Override
  public Optional<Author> findOne(Long id) {
    String sql = "SELECT * FROM authors WHERE id = ?";

    return jdbcTemplate.execute(sql, new PreparedStatementCallback<Optional<Author>>() {
      @Override
      public Optional<Author> doInPreparedStatement(PreparedStatement ps) throws SQLException {
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();

        Optional<Author> author = Optional.empty();
        while (rs.next()) {
          author = Optional.of(new Author(rs));
        }

        return author;
      }
    });
  }

  @Override
  public Integer create(Author authorData) {
    String sql = "INSERT INTO authors(first_name, last_name) VALUES (?, ?)";
    return jdbcTemplate.execute(sql, new PreparedStatementCallback<Integer>() {
      @Override
      public Integer doInPreparedStatement(PreparedStatement ps) throws SQLException {
        ps.setString(1, authorData.getFirstName());
        ps.setString(2, authorData.getLastName());
        return ps.executeUpdate();
      }
    });
  }

  @Override
  public Integer delete(Long id) {
    String sql = "DELETE FROM authors WHERE id = ?";
    return jdbcTemplate.execute(sql, new PreparedStatementCallback<Integer>() {
      @Override
      public Integer doInPreparedStatement(PreparedStatement ps) throws SQLException {
        ps.setInt(1, id.intValue());
        return ps.executeUpdate();
      }
    });
  }

  private Integer updateAuthor(Author author, Long authorId) {
    String sql = "UPDATE authors SET first_name = ?, last_name = ? WHERE id = ?";
    return jdbcTemplate.execute(sql, new PreparedStatementCallback<Integer>() {
      @Override
      public Integer doInPreparedStatement(PreparedStatement ps) throws SQLException {
        ps.setString(1, author.getFirstName());
        ps.setString(2, author.getLastName());
        if (authorId == null) {
          ps.setInt(3, author.getId().intValue());
        } else {
          ps.setInt(3, authorId.intValue());
        }
        return ps.executeUpdate();
      }
    });
  }

  @Override
  public Integer update(Author author) {
    return this.updateAuthor(author, null);
  }

  @Override
  public Integer update(Long id, Author authorData) {
    return this.updateAuthor(authorData, id);
  }
}
