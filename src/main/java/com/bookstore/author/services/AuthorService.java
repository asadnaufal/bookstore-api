package com.bookstore.author.services;

import java.util.List;
import java.util.Optional;

import com.bookstore.author.models.Author;
import com.bookstore.author.repositories.JdbcAuthorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorService implements IAuthorService {

  @Autowired
  private JdbcAuthorRepository jdbcAuthorRepository;

  @Override
  public List<Author> findAll() {
    return jdbcAuthorRepository.findAll();
  }

  @Override
  public Optional<Author> findOne(Long id) {
    return jdbcAuthorRepository.findOne(id);
  }

  @Override
  public Optional<Author> create(Author author) {
    Integer updatedRows = jdbcAuthorRepository.create(author);

    Optional<Author> newAuthor = Optional.empty();

    if (updatedRows > 0) {
      newAuthor = Optional.of(author);
    }

    return newAuthor;
  }

  @Override
  public Optional<Author> update(Long id, Author author) {
    Integer updatedRows = jdbcAuthorRepository.update(id, author);

    Optional<Author> updatedAuthor = Optional.empty();

    if (updatedRows > 0) {
      author.setId(id);
      updatedAuthor = Optional.of(author);
    }

    return updatedAuthor;
  }

  @Override
  public Optional<Author> delete(Long id) {
    Optional<Author> targetAuthor = jdbcAuthorRepository.findOne(id);

    if (targetAuthor.isPresent()) {
      jdbcAuthorRepository.delete(id);
    }

    return targetAuthor;
  }

}