package com.bookstore.author.services;

import java.util.List;
import java.util.Optional;

import com.bookstore.author.models.Author;

public interface IAuthorService {
  List<Author> findAll();
  Optional<Author> findOne(Long id);
  Optional<Author> create(Author author);
  Optional<Author> delete(Long id);
  Optional<Author> update(Long id, Author author);
}
