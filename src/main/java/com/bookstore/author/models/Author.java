package com.bookstore.author.models;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.*;
import javax.persistence.Table;

@Table(name = "authors")
@Entity
public class Author {
  private Long id;
  private String firstName;
  private String lastName;

  public Author(Long id, String firstName, String lastName) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public Author(ResultSet rs) throws SQLException {
    this.id = rs.getLong("id");
    this.firstName = rs.getString("first_name");
    this.lastName = rs.getString("last_name");
  }

  @Id
  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

}
